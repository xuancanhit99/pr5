from calculator import add, subtract, multiplication, division
firstNumber = 10
secondNumber = 5
def test_add():
 assert add(firstNumber, secondNumber) == 15
def test_subtract():
 assert subtract(firstNumber, secondNumber) == 5
def test_multiplication():
 assert multiplication(firstNumber, secondNumber) == 50

def test_division():
 assert division(firstNumber, secondNumber) == 2
